# FernandoLeonCodeChallenge

#Project created in fresh LAMP environment 
#composer
#ubuntu 16.04
#Mysql 5.7.25
#PHP 7.2.5
#Apache 2.4.18

How to Use - guide for a fresh LAMP environment set-up

1. Clone git repo (generally its a good idea to clone into your /var/www/ folder)

$ cd /var/www

$ git clone https://Fightmander@bitbucket.org/Fightmander/fleoncodechallenge.git

2. Run composer at project root to install dependencies.

$ composer install

3. open env.php file to set up your database credentials

4. run the sql migrations file to creat the mysql database and table, 
remember to put your db credentials in the command below.

$ mysql -u user -p  < database/migration/flc.sql 

5. For ease of testing make sure to create a public folder at root and give 777 permissions, so we don't have writing problems.

$ mkdir public
$ chmod -R 777 public/

6. set up your apache to point to the project.

for this in linux distributions we can go to 

 - $ cd /etc/apache2/sites-available/

then open the file 000-default.conf
 
 - $ vi 000-default.conf
 
 then we set it up to point to our project root folder, for this change the 
 
 - DocumentRoot /var/www/public -> /var/www/fleoncodechallenge/ 
 
 and

 - <Directory /var/www/public/> -> <Directory /var/www/fleoncodechallenge/>
 
 (Rember the route depends on where you cloned the project)
 
 and save and close
 
7. restart apache2

$ service apache2 restart

and that should be all! 

go to your browser  and type

http://localhost/

#Running unit testing

In order to create the tests i included phpunit. the test are stored in the test folder
and they check basically 3 things.

1. the file exists.
2. the file is readable.
3. the file has the correct content.

and in order to run the test go to root folder and type in your command line

$ ./vendor/bin/phpunit