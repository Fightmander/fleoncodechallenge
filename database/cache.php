<?php
namespace challenge;

require_once 'connection.php';


class cache extends db
{
    /**
     * function to determine if we should use the cache file or generate it
     * @return array
     */
    public function getUserTransactions()
    {
        #First, the program verifies if the cache file exists or if it's old.
        #Check the env.php file to adjust the time
        #CACHE_TIME is set to 300 seconds
        if(!file_exists('public/cache.csv') || filemtime('public/cache.csv') < time()-1*getenv("CACHE_TIME")){
            $this->cacheUserTransactions(); #if it doesn't exist we create it
        }

        #read the cached file and assign the key values
        $csv = array_map('str_getcsv', file('public/cache.csv'));
        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv); # remove column header

        return $csv;
    }

    /**
     * Function to communicate with the database and cache in a file the result
     */
    public function cacheUserTransactions()
    {
        #Since we already inherit the db connection from the extended class
        #the software just prepares to use it
        if($result = $this->PDOconnection->prepare("select * from transactions")){
            $result->execute();

            #generate file on the information we get
            $csvFileName = "public/cache.csv";
            $fp = fopen($csvFileName, 'w');

            #set-up headers
            fputcsv($fp, ["id","merchant_id","reference","status", "items","amount", "payment_method" , "created_at","modified_at"]);
            while($row = $result->fetch(\PDO::FETCH_ASSOC)){
                fputcsv($fp, $row);
            }

            fclose($fp);

        }else{
            die(PDOException. "An error occurred while extracting the database records");
        }
    }
}