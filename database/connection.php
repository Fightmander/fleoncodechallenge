<?php
namespace challenge;

require_once 'env.php';

class db
{
    public $PDOconnection;

    public function __construct()
    {
        if(!$this->PDOconnection) {
            try {
                $this->PDOconnection = new \PDO("mysql:host=".getenv('DB_HOST').";dbname=".getenv('DB_DATABASE'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'));
            } catch (PDOException $e) {
                die("PDO CONNECTION ERROR: " . $e->getMessage() . "<br/>");
            }
        }
        return $this->PDOconnection;
    }
}
