-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 13-02-2019 a las 07:03:52
-- Versión del servidor: 5.7.21
-- Versión de PHP: 7.2.4

CREATE DATABASE flc;

USE flc;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `flc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `merchant_id` int(10) UNSIGNED NOT NULL,
  `reference` int(10) UNSIGNED NOT NULL,
  `status` enum('authorized','pending','cancelled') NOT NULL,
  `items` text NOT NULL,
  `amount` float NOT NULL,
  `payment_method` varchar(256) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `transactions`
--

INSERT INTO `transactions` (`id`, `merchant_id`, `reference`, `status`, `items`, `amount`, `payment_method`, `created_at`, `modified_at`) VALUES
(1, 1, 303266567, 'authorized', '[{\"item\":\"tacos\",\"quantity\":2}]', 1000, 'CC 4111 1111 1111 1111', '2019-02-12 20:49:37', '2019-02-03 20:49:37'),
(2, 1, 325632014, 'pending', '[{\"item\":\"tacos\",\"quantity\":3}]', 100, 'CC 1111 1111 1111', '2019-02-13 00:47:07', '2019-02-13 00:47:07'),
(5, 1, 235698745, 'cancelled', '[{\"item\":\"carro\",\"quantity\":1}]', 50000, 'CC 2222 5555 2222 5555', '2019-02-13 00:54:46', '2019-03-01 00:54:46'),
(6, 1, 1234567897, 'authorized', '[{\"item\":\"subscription\",\"quantity\":1}]', 1000, 'CC 8888 9999 7777 8888', '2019-02-13 00:54:46', '2019-02-05 00:54:46'),
(7, 1, 303266562, 'pending', '[{\"item\":\"tacos\",\"quantity\":2}]', 1000, 'CC 4111 1111 1111 1111', '2019-02-12 20:49:37', '2019-02-12 20:49:37'),
(8, 1, 325636014, 'pending', '[{\"item\":\"tacos\",\"quantity\":3}]', 100, 'CC 1111 1111 1111', '2019-02-13 00:47:07', '2019-02-21 06:00:00'),
(9, 1, 235698445, 'cancelled', '[{\"item\":\"carro\",\"quantity\":2},{\"item\":\"moto\",\"quantity\":2}]', 888, 'CC 2222 5555 2222 5555', '2019-02-06 00:54:46', '2019-02-07 00:54:46'),
(10, 1, 1234567892, 'pending', '[{\"item\":\"subscription\",\"quantity\":1}]', 1000, 'CC 8888 9999 7777 8888', '2019-02-13 00:54:46', '2019-02-14 00:54:46');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
