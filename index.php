<?php
namespace challenge;

require_once 'database/cache.php';

$cache = new cache(); #call the cache class
$array = $cache->getUserTransactions(); #generates the cache
$result = $array; #assign the values to display in case no post is sent

if( !empty($_POST['status']) || !empty($_POST['reference']) || !empty($_POST["items"]) || !empty($_POST["amount"]) || !empty($_POST["payment_method"]) || !empty($_POST["date"])){

    #if post request is sent get the values
    $search = [
        "status" => $_POST["status"],
        "reference" => $_POST["reference"],
        "items" => $_POST["items"],
        "amount" => $_POST["amount"],
        "payment_method" => $_POST["payment_method"],
        "modified_at" => $_POST["date"]
    ];

    #filter the values of every row based on the values from post
    #big O(n)
    $result = array_filter($array, function ($item) use ($search) {
        foreach ($search as $key => $value){
            if(stripos($item[$key], $value) !== false){
                return true;
            }
        }
        return false;
    });

}

#call the view to display the results
include_once 'views/transactions.php';


