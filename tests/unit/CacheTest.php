<?php

use PHPUnit\Framework\TestCase;

class CacheTest extends TestCase
{
    /**
     * Test case to verify if the file exists
     */
    public function testCacheFile()
    {
        $this->assertFileExists("public/cache.csv");
    }

    /**
     * test case to verify if the file is readable
     */
    public function testReadableFile()
    {
        $this->assertFileIsReadable("public/cache.csv");
    }

    /**
     * test case to verify if the cache file has the correct array values
     */
    public function testFileValues()
    {
        $csv = array_map('str_getcsv', file('public/cache.csv'));
        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });
        array_shift($csv);

        $this->assertArrayHasKey('status', $csv[0]);
        $this->assertArrayHasKey('reference', $csv[0]);
        $this->assertArrayHasKey('items', $csv[0]);
        $this->assertArrayHasKey('amount', $csv[0]);
        $this->assertArrayHasKey('payment_method', $csv[0]);
        $this->assertArrayHasKey('modified_at', $csv[0]);
    }

}