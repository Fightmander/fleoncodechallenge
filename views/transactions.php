<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">

    <title>Fernando Leon Code Challenge</title>
</head>
<body>

<div class="container">
    <div class="row">
        <h1>Challenge: DB cache</h1>
    </div>

    <div class="col-lg-12">
        <h3>User Transactions</h3>
    </div>
    <form method="post" action="/index.php">
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="reference">Reference</label>
                <input type="text" class="form-control" id="reference" name="reference">
            </div>
            <div class="form-group col-md-4">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="status">
                <option value="">Status</option>
                <option value="authorized">Authorized</option>
                <option value="pending">Pending</option>
                <option value="cancelled">Cancelled</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="items">Items</label>
                <input type="text" class="form-control" id="items" name="items">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="amount">Amount</label>
                <input type="text" class="form-control" id="amount" name="amount">
            </div>
            <div class="form-group col-md-4">
                <label for="payment_method">Payment Method</label>
                <input type="text" class="form-control" id="payment_method" name="payment_method">
            </div>
            <div class="form-group col-md-4">
                <label for="date">Date</label>
                <input type="text" class="form-control" id="date" name="date">
            </div>
        </div>
        <div class="form-row">
            <button class="form-group btn btn-primary" type="submit">Search</button>
        </div>
    </form>
    <div class="row">
        <div class="col-lg-12">
            <table class="table mt-3">
                <thead>
                <tr>
                    <th scope="col">Reference</th>
                    <th scope="col">Status</th>
                    <th scope="col">Items</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Payment Method</th>
                    <th scope="col">Modified Date</th>
                </tr>
                </thead>
                <tbody>

                <?php
                if(isset($result)){
                    foreach($result as $row){
                        echo "<tr>
                        <td>$row[reference]</td>
                        <td>$row[status]</td>
                        <td>";
                        //var_dump(json_decode(json_encode(array(array("item" => "pintura", "quantity" => 2),array("item" => "perro", "quantity" => 1))), true));
                        foreach(json_decode($row["items"], true) as $key => $item){
                            echo $item["item"]. " - " . $item["quantity"]. "<br>";
                        }
                        echo "</td>
                        <td>$row[amount]</td>
                        <td>$row[payment_method]</td>
                        <td>";
                        echo date('Y-m-d', strtotime($row["modified_at"]));
                        echo "</td>
                    </tr>";
                    }
                }

                ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/js/bootstrap.min.js" integrity="sha384-7aThvCh9TypR7fIc2HV4O/nFMVCBwyIUKL8XCtKE+8xgCgl/PQGuFsvShjr74PBp" crossorigin="anonymous"></script>
</body>
</html>